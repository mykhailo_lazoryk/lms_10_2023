from django.db.models import Q
from django.http import HttpResponse, HttpRequest, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from webargs import fields
from webargs.djangoparser import use_args

from students.forms import StudentCreateForm
from students.models import Student
from students.utils.helpers import format_records


def index(request: HttpRequest) -> HttpResponse:
    # TODO add logic for user request
    return HttpResponse("Hello from my first view!!!")


@use_args(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    },
    location="query",
)
def get_students(request: HttpRequest, params) -> HttpResponse:
    students = Student.objects.all()

    search_fields = ["last_name", "first_name", "email"]

    for name, value in params.items():
        if name == "search_text":
            or_filter = Q()
            for field in search_fields:
                # accumulator of filtering conditions
                or_filter |= Q(**{f"{field}__icontains": value})
            students = students.filter(or_filter)
        else:
            students = students.filter(**{name: value})

    result = format_records(students)
    return HttpResponse(result)


@csrf_exempt
def create_student(request: HttpRequest) -> HttpResponse:
    if request.method == "POST":
        form = StudentCreateForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect("/students")

    elif request.method == "GET":
        form = StudentCreateForm()

    form_html = f"""
    <form method="post">
        {form.as_p()}
        <button type="submit" value="Submit">Create</button>
    </form>
    """
    return HttpResponse(form_html)


# CI/CD
# Continues Integration/ Continuous Deployment


# 80
# 120
