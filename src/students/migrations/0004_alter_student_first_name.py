# Generated by Django 4.2.5 on 2023-10-12 18:25

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("students", "0003_alter_student_email"),
    ]

    operations = [
        migrations.AlterField(
            model_name="student",
            name="first_name",
            field=models.CharField(
                blank=True,
                max_length=120,
                null=True,
                validators=[django.core.validators.MinLengthValidator(1)],
            ),
        ),
    ]
