from django.urls import path

from students.views import index, get_students, create_student

urlpatterns = [
    path("index/", index, name="index"),
    path("", get_students, name="get_students"),
    path("create/", create_student, name="create_student"),
]

# /students/ -> all
# /students/4/ -> id=4
# /students/update/4/ -> id=4 to update
# /students/delete/4/ -> id=4 to delete
